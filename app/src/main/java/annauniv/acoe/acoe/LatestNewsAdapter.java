package annauniv.acoe.acoe;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sidharth Yatish on 27-08-2016.
 */
public class LatestNewsAdapter extends RecyclerView.Adapter<LatestNewsAdapter.ViewHolder> {
    private ArrayList<String> newsDataSet;

    @Override
    public LatestNewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_latest_news,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String news =newsDataSet.get(position);
        holder.newsText.setText(news);
    }
    public void add(int position, String item) {
        newsDataSet.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = newsDataSet.indexOf(item);
      newsDataSet.remove(position);
        notifyItemRemoved(position);
    }
    @Override
    public int getItemCount() {
        return newsDataSet.size();
    }
    public LatestNewsAdapter(ArrayList<String> myDataset) {
        newsDataSet = myDataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView newsText;

        public ViewHolder(View itemView) {
            super(itemView);
            newsText= (TextView) itemView.findViewById(R.id.latest_news_text_item);

        }
    }
}
