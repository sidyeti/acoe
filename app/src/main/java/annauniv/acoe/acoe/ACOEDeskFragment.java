package annauniv.acoe.acoe;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ACOEDeskFragment extends Fragment {
    TextView nameText;
    TextView addressText;
    ImageView imageView;
    TextView contentText;

    public ACOEDeskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_acoedesk, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        nameText= (TextView) getActivity().findViewById(R.id.acoe_desk_name);
        nameText.setText(getString(R.string.acoe_desk_name));
        addressText= (TextView) getActivity().findViewById(R.id.acoe_desk_address);
        addressText.setText(Html.fromHtml(getString(R.string.acoe_desk_address)));
        contentText= (TextView) getActivity().findViewById(R.id.acoe_desk_content);
        contentText.setText(Html.fromHtml(getString(R.string.acoe_desk_content)));
    }
}
