package annauniv.acoe.acoe;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sidharth Yatish on 06-10-2016.
 */

public class DownLoadLinksAdapter extends RecyclerView.Adapter<DownLoadLinksAdapter.DownloadLinksViewHolder> {
    private ArrayList<DownloadLinks> dLinksDataSet;
    private Context context;
    DownLoadLinksAdapter(ArrayList<DownloadLinks> links,Context context){
        dLinksDataSet=links;
        this.context=context;
    }

    public DownLoadLinksAdapter(ArrayList<DownloadLinks> importantLinksList) {
        this.dLinksDataSet=importantLinksList;
    }

    @Override
    public DownLoadLinksAdapter.DownloadLinksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_download_links,parent,false);
        return new DownLoadLinksAdapter.DownloadLinksViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DownloadLinksViewHolder holder, int position) {
        String linkText=dLinksDataSet.get(position).getText();
        holder.linksText.setText(linkText);
    }

    @Override
    public int getItemCount() {
        return dLinksDataSet.size();
    }
    public void add(int position,DownloadLinks item){
        dLinksDataSet.add(position,item);
        notifyItemInserted(position);
    }

    public class DownloadLinksViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView linksText;
        public String impUrl;
        public CardView impLinksCardView;
        public DownloadLinksViewHolder(View itemView) {
            super(itemView);
            linksText= (TextView) itemView.findViewById(R.id.down_important_links_text_item);
            impLinksCardView= (CardView) itemView.findViewById(R.id.download_links_card_view);
            impLinksCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            impUrl=dLinksDataSet.get(getAdapterPosition()).getUrl();
            if(impUrl.endsWith(".pdf")){
                Intent i=new Intent(context,PDFViewerActivity.class);
                i.putExtra("url",impUrl);
                context.startActivity(i);
            }
            else{
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(impUrl));
                context.startActivity(i);
            }
        }
    }

}
