package annauniv.acoe.acoe;

/**
 * Created by Sidharth Yatish on 06-10-2016.
 */

public class DownloadLinks {
    String text;
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
