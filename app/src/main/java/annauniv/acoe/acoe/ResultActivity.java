package annauniv.acoe.acoe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ResultActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ResultsAdapter resultsAdapter;
    private ArrayList<Result> resultArrayList;
    private TextView emptyTextView;
    private String rollNumber;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        rollNumber=getIntent().getStringExtra("rollNumber");
        recyclerView= (RecyclerView) findViewById(R.id.result_recycler_view);
        recyclerView.setHasFixedSize(true);
        emptyTextView= (TextView) findViewById(R.id.emptyProgressResults);
        resultArrayList=new ArrayList<>();
        layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        resultsAdapter=new ResultsAdapter(resultArrayList);
        if(resultArrayList.isEmpty()){
            recyclerView.setVisibility(View.GONE);
            emptyTextView.setVisibility(View.VISIBLE);
        }
        getData();
        recyclerView.setAdapter(resultsAdapter);
        Toolbar toolbar= (Toolbar) findViewById(R.id.result_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Results");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        assert toolbar != null;
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
    void getData(){
        String url="http://www.haha.cegtechforum.com/getResults.php";
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("RESULT_RESPONSE","Got a response");
                recyclerView.setVisibility(View.VISIBLE);
                emptyTextView.setVisibility(View.GONE);
                try {
                    JSONObject jb=new JSONObject(response);
                    JSONArray jsonArray= jb.getJSONArray("data");

                    for(int i=0;i<jsonArray.length();i++){
                        Result result=new Result();
                        JSONObject data=jsonArray.getJSONObject(i);
                        result.setGrade(data.getString("grade"));
                        result.setSubject(data.getString("subject"));
                        Log.d("Looping",result.getGrade()+" "+result.getSubject());
                        resultArrayList.add(result);
                    }
                    resultsAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            emptyTextView.setText("Could not fetch results.. try later");
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", rollNumber);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
