package annauniv.acoe.acoe;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentSectionFragment extends Fragment {

    private EditText registerNumberEditText;
    private EditText passwordEditText;
    private Button loginButton;

    public StudentSectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_student_section, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerNumberEditText= (EditText) getActivity().findViewById(R.id.register_number_edit_text);
        passwordEditText= (EditText) getActivity().findViewById(R.id.password_edit_text);
        loginButton= (Button) getActivity().findViewById(R.id.login_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reg=registerNumberEditText.getText().toString().trim();
                String pass=passwordEditText.getText().toString().trim();
                if(reg.length()==0||pass.length()==0){
                    Toast.makeText(getActivity().getApplicationContext(), "Enter both fields", Toast.LENGTH_SHORT).show();
                }
                else{
                    getLogin(reg,pass);
                }
            }
        });
    }
    private void getLogin(final String userName, final String password) {

        final ProgressDialog progressDialog=ProgressDialog.show(getActivity(),"Signing in","please wait",true);
        String url = "http://www.haha.cegtechforum.com/login.php";
        // Toast.makeText(LoginActivity.this, url, Toast.LENGTH_SHORT).show();
        StringRequest getStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    progressDialog.dismiss();
                    JSONObject jb=new JSONObject(response);
                    String code=jb.getString("responseCode");
                    Log.d("IN_LOGIN","logged in response code : "+code);
                  //  Toast.makeText(getActivity().getApplicationContext(), "Response code"+code, Toast.LENGTH_SHORT).show();

                    if(code.equals("0"))
                        Toast.makeText(getActivity().getApplicationContext(), "Invalid credentials", Toast.LENGTH_SHORT).show();
                    else{
                        Intent i=new Intent(getActivity().getApplicationContext(),ResultActivity.class);
                        i.putExtra("rollNumber",userName);
                        registerNumberEditText.setText("");
                        passwordEditText.setText("");
                        startActivity(i);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getActivity().getApplicationContext(), "RESPONSE ERROR" + error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", userName);
                params.put("password", password);
                return params;
            }
        };
        getStringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        requestQueue.add(getStringRequest);
    }

}
