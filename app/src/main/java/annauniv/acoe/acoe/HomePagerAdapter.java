package annauniv.acoe.acoe;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Sidharth Yatish on 27-08-2016.
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public HomePagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                LatestNewsFragment newsFragment=new LatestNewsFragment();
                return newsFragment;
            case 1:
                EventsFragment eventsFragment=new EventsFragment();
                return eventsFragment;
            default:
                return null;
        }
    }
}
