package annauniv.acoe.acoe;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sidharth Yatish on 11-09-2016.
 */
public class ImportantLinksAdapter extends RecyclerView.Adapter<ImportantLinksAdapter.LinksViewHolder>{

    private ArrayList<ImportantLink> linksDataSet;
    private Context context;

    ImportantLinksAdapter(ArrayList<ImportantLink> links,Context context){
        linksDataSet=links;
        this.context=context;
    }

    public ImportantLinksAdapter(ArrayList<ImportantLink> importantLinksList) {
        this.linksDataSet=importantLinksList;
    }

    @Override
    public LinksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_important_links,parent,false);
        return new LinksViewHolder(v);
    }

    @Override
    public void onBindViewHolder(LinksViewHolder holder, int position) {
        String linkText=linksDataSet.get(position).getText();
        holder.linksText.setText(linkText);
    }

    @Override
    public int getItemCount() {
       return linksDataSet.size();
    }
    public void add(int position,ImportantLink item){
        linksDataSet.add(position,item);
        notifyItemInserted(position);
    }
    public void remove(ImportantLink item){
        int position=linksDataSet.indexOf(item);
        linksDataSet.remove(position);
        notifyItemRemoved(position);
    }

    public class LinksViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView linksText;
        public String impUrl;
        public CardView impLinksCardView;
        public LinksViewHolder(View itemView) {
            super(itemView);
            linksText= (TextView) itemView.findViewById(R.id.important_links_text_item);
            impLinksCardView= (CardView) itemView.findViewById(R.id.links_card_view);
            impLinksCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            impUrl=linksDataSet.get(getAdapterPosition()).getUrl();
            if(impUrl.endsWith(".pdf")){
                Intent i=new Intent(context,PDFViewerActivity.class);
                i.putExtra("url",impUrl);
                context.startActivity(i);
            }
            else{
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(impUrl));
                context.startActivity(i);
            }
        }
    }
}
