package annauniv.acoe.acoe;

/**
 * Created by Sidharth Yatish on 02-09-2016.
 */
public class Palette {
    private String name;
    private String hexValue;
    private int intValue;
    Palette(int intValue){
        this.intValue=intValue;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHexValue() {
        return hexValue;
    }

    public void setHexValue(String hexValue) {
        this.hexValue = hexValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }
}
