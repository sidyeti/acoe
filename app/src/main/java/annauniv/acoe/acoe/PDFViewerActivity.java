package annauniv.acoe.acoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PDFViewerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viewer);
        String url=getIntent().getStringExtra("url");
        WebView wb= (WebView) findViewById(R.id.pdf_web_view);
        wb.getSettings().setJavaScriptEnabled(true);
        wb.setWebViewClient(new WebViewClient());
        Toolbar toolbar= (Toolbar) findViewById(R.id.pdf_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("PDF Viewer");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        assert toolbar != null;
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        wb.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+url);

    }
}
