package annauniv.acoe.acoe;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView= (NavigationView) findViewById(R.id.navigation_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                Class fragmentClass;
                Fragment fragment=new HomeFragment();
                switch(item.getItemId()){
                    case R.id.nav_home:
                        fragmentClass=HomeFragment.class; break;
                    case R.id.nav_important_items:
                        fragmentClass=ImportantLinksFragment.class; break;
                    case R.id.nav_download_section:
                        fragmentClass=DownloadsFragment.class; break;
                    case R.id.nav_students_section:
                        fragmentClass=StudentSectionFragment.class; break;
                    case R.id.nav_acoe_desk:
                        fragmentClass=ACOEDeskFragment.class;break;
                    default:
                        fragmentClass=HomeFragment.class;
                }
                try {
                    fragment= (Fragment) fragmentClass.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame,fragment)
                        .commit();
                item.setChecked(true);
                toolbar.setTitle(item.getTitle());
                drawerLayout.closeDrawers();

                return true;

            }
        });
        ActionBarDrawerToggle actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        actionBarDrawerToggle.syncState();
        if(savedInstanceState==null){
            HomeFragment homeFragment=new HomeFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_frame,homeFragment)
                    .commit();
        }

    }
}
