package annauniv.acoe.acoe;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {
    private int page=1;
    private List<Event> eventList;
    private EventsAdapter adapter;ProgressBar emptyProgress;  RecyclerView recyclerView;

    private String months[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView= (RecyclerView) getActivity().findViewById(R.id.events_recycler_view);
//        recyclerView.setHasFixedSize(true);
        emptyProgress= (ProgressBar) getActivity().findViewById(R.id.emptyProgressevents);
        LinearLayoutManager llm=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        eventList=new ArrayList<>();
        eventList.add(null);
        if(eventList.isEmpty()||eventList.size()<2){
            recyclerView.setVisibility(View.GONE);
            emptyProgress.setVisibility(View.VISIBLE);
        }
        getData(page);

        adapter=new EventsAdapter(eventList,recyclerView,getContext());
        recyclerView.setAdapter(adapter);

        adapter.setOnLoadMoreListener(new EventsAdapter.OnLoadMoreListener(){
            @Override
            public void onLoadMore() {
                eventList.add(null);
                adapter.notifyItemInserted(eventList.size() - 1);
                page++;
                getData(page);
            }
        });

    }
    private void getData(final int page){
    String url="http://www.haha.cegtechforum.com/getEvents.php?page=";
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url + String.valueOf(page), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        emptyProgress.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    eventList.remove(null);
                        adapter.notifyItemRemoved(eventList.size());
                        try {
                            int responseCode= Integer.parseInt(response.getString("responseCode"));
                            int count= Integer.parseInt(response.getString("count"));
                            if(responseCode==0&&count==0){
                                Toast.makeText(getActivity().getApplicationContext(), "No more events", Toast.LENGTH_SHORT).show();
                            }
                            else if(responseCode==0){
                                Toast.makeText(getActivity().getApplicationContext(), "Please try again", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                JSONArray events=response.getJSONArray("data");
                                for(int i=0;i<events.length();i++){
                                Event newEvent=new Event();
                                    JSONObject currentEvent=events.getJSONObject(i);
                                    String date=currentEvent.getString("date");
                                    String desc=currentEvent.getString("event");
                                    String[] dateParts=date.split("-");
                                    newEvent.setDay(dateParts[2]);
                                    newEvent.setMonth(months[Integer.parseInt(dateParts[1])-1]);
                                    newEvent.setDescription(desc);
                                    eventList.add(newEvent);
                                    adapter.notifyItemInserted(eventList.size());
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(getActivity().getApplicationContext(), "Error while downloading data..", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                        adapter.setLoaded();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("VOLLEY_ERROR_EVENTS",error.toString());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestqueue= Volley.newRequestQueue(getContext());
        requestqueue.add(jsonObjectRequest);
    }
}
