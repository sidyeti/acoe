package annauniv.acoe.acoe;

/**
 * Created by Sidharth Yatish on 11-09-2016.
 */
public class ImportantLink {
    String text;
    String url;

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
