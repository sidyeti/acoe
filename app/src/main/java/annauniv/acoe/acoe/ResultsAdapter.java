package annauniv.acoe.acoe;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sidharth Yatish on 03-10-2016.
 */

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ResultsViewHolder> {
    private ArrayList<Result> resultsDataSet;

    public ResultsAdapter(ArrayList<Result> results){
        this.resultsDataSet=results;
    }
    @Override
    public ResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_results,parent,false);
        return new ResultsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ResultsViewHolder holder, int position) {
        String subjectText=resultsDataSet.get(position).getSubject();
        String gradeText=resultsDataSet.get(position).getGrade();
        holder.subjectTextView.setText(subjectText);
        holder.gradeTextView.setText(gradeText);
    }

    @Override
    public int getItemCount() {
        return resultsDataSet.size();
    }

    public class ResultsViewHolder extends RecyclerView.ViewHolder{

        public TextView subjectTextView;
        public TextView gradeTextView;
        public ResultsViewHolder(View itemView) {
            super(itemView);
            subjectTextView= (TextView) itemView.findViewById(R.id.subject_name_text_view);
            gradeTextView= (TextView) itemView.findViewById(R.id.grade_text_view);
        }
    }
}
