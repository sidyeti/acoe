package annauniv.acoe.acoe;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sidharth Yatish on 02-09-2016.
 */
public class EventsAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private List<Event> eventsList;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private Context context;
    private OnLoadMoreListener onLoadMoreListener;
    private List<Palette> palettes;

    public EventsAdapter(List<Event> events,RecyclerView recyclerView,Context context){
        eventsList=events;
        this.context=context;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }

        //Add palettes

        this.palettes= new ArrayList<>();

        palettes.add(new Palette(Color.parseColor("#E26E67")));
        palettes.add(new Palette(Color.parseColor("#F1C40F")));
        palettes.add(new Palette(Color.parseColor("#6C7A89")));
        palettes.add(new Palette(Color.parseColor("#F29B34")));
        palettes.add(new Palette(Color.parseColor("#00B5B5")));
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh=null;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_item_events, parent, false);

            vh = new EventViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }
    @Override
    public int getItemViewType(int position) {
        return eventsList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }
    public void setLoaded() {
        loading = false;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Palette palette=palettes.get(position%palettes.size());
        if(holder instanceof EventViewHolder){
            Event event=eventsList.get(position);
            EventViewHolder eventViewHolder= (EventViewHolder) holder;
            eventViewHolder.dateTextView.setText(event.getDay());
            eventViewHolder.monthTextView.setText(event.getMonth());
            eventViewHolder.descTextView.setText(event.getDescription());
            eventViewHolder.eventsCardView.setBackgroundColor(palette.getIntValue());
        }
        else if(holder instanceof ProgressViewHolder){
            ProgressViewHolder pHolder = (ProgressViewHolder) holder;
            pHolder.progressBar.setIndeterminate(true);

        }
    }

    @Override
    public int getItemCount() {
        return eventsList.size();
    }
    public class EventViewHolder extends RecyclerView.ViewHolder{
        public TextView dateTextView;
        public TextView monthTextView;
        public TextView descTextView;
        public CardView eventsCardView;
        public EventViewHolder(View itemView) {
            super(itemView);
            dateTextView= (TextView) itemView.findViewById(R.id.date_text_view);
            monthTextView= (TextView) itemView.findViewById(R.id.month_text_view);
            descTextView= (TextView) itemView.findViewById(R.id.events_desc_view);
            eventsCardView= (CardView) itemView.findViewById(R.id.events_card_view);
        }
    }
    public class ProgressViewHolder extends RecyclerView.ViewHolder{
        private ProgressBar progressBar;
        public ProgressBar getProgressBar() {
            return progressBar;
        }
        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar= (ProgressBar) itemView.findViewById(R.id.progressBar);
        }

    }public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

}
