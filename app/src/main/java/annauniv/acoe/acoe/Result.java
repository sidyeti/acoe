package annauniv.acoe.acoe;

/**
 * Created by Sidharth Yatish on 03-10-2016.
 */

public class Result {

    String subject;
    String grade;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
