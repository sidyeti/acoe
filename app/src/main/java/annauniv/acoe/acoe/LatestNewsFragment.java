package annauniv.acoe.acoe;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class LatestNewsFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LatestNewsAdapter newsAdapter;
    private ArrayList<String> newsArrayList;
    private ProgressBar emptyProgressBar;
    public LatestNewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_latest_news, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView= (RecyclerView) getActivity().findViewById(R.id.latest_news_recycler_view);
        recyclerView.setHasFixedSize(true);
        emptyProgressBar= (ProgressBar) getActivity().findViewById(R.id.emptyProgress);
        newsArrayList=new ArrayList<>();
        layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        newsAdapter=new LatestNewsAdapter(newsArrayList);
        if(newsArrayList.isEmpty()){
            recyclerView.setVisibility(View.GONE);
            emptyProgressBar.setVisibility(View.VISIBLE);
        }
        getData();
        recyclerView.setAdapter(newsAdapter);


    } public void getData() {
        StringRequest stringRequest=new StringRequest(Request.Method.GET, "https://acoe.annauniv.edu/event/event.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                emptyProgressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                Document document= Jsoup.parse(response);

                Elements fonts=document.getElementsByTag("font");
                for(int i=0;i<fonts.size();i++){
                    Element x=fonts.get(i);
                    Elements p=x.getElementsByTag("p");
                    if(p.get(0).text().length()!=0) newsArrayList.add(p.get(0).text());
                }
                newsAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                emptyProgressBar.setVisibility(View.GONE);
                String errorReason=null;
                if(error instanceof TimeoutError)
                    errorReason="Kindly try again later...";
                else if(error instanceof NoConnectionError)
                    errorReason="No network available...";
                else
                    errorReason="Kindly restart the application and try again..";
                Toast.makeText(getActivity().getApplicationContext(), errorReason, Toast.LENGTH_SHORT).show();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue= Volley.newRequestQueue(getActivity().getApplicationContext());
        requestQueue.add(stringRequest);
    }

}
